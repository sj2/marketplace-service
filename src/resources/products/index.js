'use strict';

var controller = require('./products.controller');
var router = require('koa-router')();

router.get('/', controller.index);
router.get("/bestRate", controller.bestRate);
module.exports = router.routes();
