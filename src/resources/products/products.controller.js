'use strict';

const _ = require("lodash");

const products = require('./products.json');
const bestRateProduct = _.sortBy(products, p => -p.rate)[0];

exports.index = function*(next) {
	this.status = 200;
  this.body = products;
};

exports.bestRate = function*(next) {
	this.status = 200;
	this.body = bestRateProduct;
};
