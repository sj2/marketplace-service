'use strict';

var app = require('../../server');
var request = require('supertest').agent(app.listen());

var expect = require('chai').expect;
var should = require('should');

describe('GET /api/products', function(){
  it('should respond with 200 type Array', function(done){
    request
    .get('/api/products')
    .expect(200, function(err, res) {
    	expect(Array.isArray(res.body)).to.be.true;
    	done();
    });
  });
});

describe("GET /api/products/bestRate", () => {
  it("should return the product with the best rate", (done) => {
    const bestRate = 2.6299014;

    request
    .get("/api/products/bestRate")
    .expect(200, (err, res) => {
      expect(res.body.rate).to.equal(bestRate);
      done();
    });
  });
});
